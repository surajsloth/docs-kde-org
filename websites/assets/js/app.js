require('../css/app.scss');
require('bootstrap');

const $ = require('jquery');
require('jquery-ui/ui/widgets/autocomplete.js');

$(function() {
    $('#search_bar_search_term').autocomplete({
        source: $('#autocomplete-data').data('autocomplete').split(','),
        select: (event, ui) => {
            $('#search_bar_search_term').val(ui.item.value);
            $('form[name=search_bar]').submit();
        }
    });
});
