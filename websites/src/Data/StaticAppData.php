<?php

namespace App\Data;

class StaticAppData
{
    const check_array = [
        "kcontrol/",
        "kcontrol5/",
        "khelpcenter/", // do we really need this?
        "kioslave/",
        "kioslave5/",
    ];

    /*
        paths which are meant to be ignored. Odd things
        happening in the doc(s) dirs.
    */
    const ignore_array = [
        "applications/glossary",
        "kdelibs/common",
        "kdelibs/kdelibs",
        "kdelibs/api",
        "kdelibs/kjs",
        "kdelibs/kross",
        "kdelibs/kded4",
        "kdelibs/checkXML",
        "kdelibs/kdeoptions",
        "kdelibs/kioslave", #needed to get no kioslave in Applications box
        "kdelibs/kjscmd",
        "kdelibs/kde4-config",
        "kdelibs/makekdewidgets",
        "kdelibs/kbuildsycoca4",
        "kdelibs/preparetips",
        "kdelibs/qtoptions",
        "kdelibs/meinproc4",
        "kdelibs/kconfig_compiler",
        "kdelibs/kdeinit4",
        "kdelibs/kcookiejar4",
        "kde-runtime/api",
        "kde-runtime/kioslave", #needed to get no kioslave in Applications box
        "kde-runtime/glossary",
        "kde-runtime/nepomuk",
        "kde-workspace/plasma-desktop/tools",
        "kdegames/ksirk/NewDocSnapshots",
        "kdepim/akonadi_followupreminder_agent", #temporarily ignore empty docs
        "kdepim/akonadi_notes_agent",
        "kdepim/contactthemeeditor",
        "kdepim/headerthemeeditor",
        "kdepim/sieveeditor", # end ignore zone
        "kdepim/common",
        "kdepim/kioslave", #needed to get no kioslave in Applications box
        "kdepim/api",
        "kdepimlibs/kioslave", #needed to get no kioslave in Applications box
        "kdepimlibs/api",
        "kdevelop/api",
        "kdegames/api",
        "kdegames/ksirk/ksirkskineditor",
        "kdeedu/api",
        "kdeedu/kig/scripting-api",
        "kdevelop/tools",
        "kdemultimedia/kioslave", #needed to get no kioslave in Applications box
        "kdesdk/scripts",
        "kdesdk/kmtrace",
        "kdesdk/poxml",
        "kdeutils/superkaramba",
        "kdeutils/superkaramba/api",
        "kdeutils/superkaramba/faq",
        "kdewebdev/kommander",
        "extragear-network/kioslave", #needed to get no kioslave in Applications box
        "extragear-base/kappfinder",
        "extragear-graphics/digikam/project",
        "calligra/api",
        "calligra/krita",
    ];

    const subdir_array = [];

    const package_no_application_array = array("kdepimlibs", "kdepim-runtime");

    const full_language_name = [
        "en" => "English",
        "ca" => "Català",
        "fr" => "Français",
        "de" => "Deutsch",
        "uk" => "українська",
    ];
}

?>
