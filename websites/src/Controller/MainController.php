<?php

namespace App\Controller;

use App\Entity\Search;
use App\Form\SearchBarType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Data\GeneratedData;
use App\Data\StaticAppData;
use Symfony\Component\Cache\Adapter\FilesystemAdapter;
use Symfony\Contracts\Translation\TranslatorInterface;

class MainController extends AbstractController
{
    private $programs_docs;

    /**
     * Return the homepage
     * @Route("/{_locale}", name="index", defaults={"_locale": "en"}, requirements={"_locale": "%app.locales%"})
     * @param string $_locale
     * @param TranslatorInterface $translator
     * @return Response
     */
    public function indexAction(string $_locale, TranslatorInterface $translator): Response
    {
        return $this->render('index.html.twig', [
            'modules_programs' => GeneratedData::modules_programs,
            'lang' => $_locale,
            'search_form' => $this->createSearchForm()->createView(),
            'all_application_names' => $this->getAllApplicationsName($_locale, $translator),
            'languages' => GeneratedData::languagelist,
            'branch_descriptions' => GeneratedData::branch_description,
        ]);
    }

    /**
     * Return the dropdown menu in case the user has noScript 
     * @Route("/{_locale}/menu", name="menu", defaults={"_locale": "en"}, requirements={"_locale": "%app.locales%"})
     * @param string $_locale
     * @param TranslatorInterface $translator
     * @return Response
     */
    public function menuAction(string $_locale, TranslatorInterface $translator): Response
    {
        return $this->render('menu.html.twig', [
            'lang' => $_locale,
            'search_form' => $this->createSearchForm()->createView(),
            'all_application_names' => $this->getAllApplicationsName($_locale, $translator),
            'languages' => GeneratedData::languagelist,
        ]);
    }


    /**
     * @Route("/{_locale}/search", name="search", defaults={"_locale": "en"}, requirements={"_locale":  "%app.locales%"})
     * @param string $_locale
     * @param Request $request
     * @param TranslatorInterface $translator
     * @return Response
     * @throws \Psr\Cache\InvalidArgumentException
     */
    public function searchAction(string $_locale, Request $request, TranslatorInterface $translator): Response
    {
        // create search form
        $search = new Search();
        $form = $this->createForm(SearchBarType::class, $search, [
            'action' => $this->generateUrl('search'),
        ]);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            // do the search with the provided search term
            $searchTerm = $search->getSearchTerm();
            $cache = new FilesystemAdapter();
            $searchResult = [];
            $searchTermSimple = str_replace('/', '-', $searchTerm);
            $searchResultCache = $cache->getItem('search.' . $_locale . '.' . $searchTermSimple);
            $searchResultCache->expiresAt(new \DateTime('tomorrow'));

            if (!$searchResultCache->isHit()) {
                foreach (GeneratedData::modules_programs as $module => $moduleContent) {
                    foreach ($moduleContent as $submodule) {
                        $array = explode('/', $submodule);
                        if (array_key_exists($_locale, GeneratedData::programs_docs[$submodule])) {
                            $branch_available = GeneratedData::programs_docs[$submodule][$_locale];

                            if (count($array) == 1 && strpos(($displayName = $translator->trans($submodule, [], "modules")), $searchTerm) !== false) {
                                $searchResult[$module][$submodule] = ["branch" => $branch_available];
                            } elseif (count($array) == 2
                                && strpos(($displayName = $translator->trans($array[1], [], "modules")), $searchTerm) !== false) {
                                $searchResult[$module][$array[0]][$array[1]] = ["branch" => $branch_available];
                            }
                        }
                    }
                }
                $searchResultCache->set($searchResult);
                $cache->save($searchResultCache);
            } else {
                $searchResult = $searchResultCache->get();
            }


            return $this->render('search.html.twig', [
                'lang' => $_locale,
                'search_term' => $search->getSearchTerm(),
                'search_result' => $searchResult,
                'branch_descriptions' => GeneratedData::branch_description,
                'all_application_names' => $this->getAllApplicationsName($_locale, $translator),
                'search_form' => $this->createSearchForm()->createView(),
                'languages' => GeneratedData::languagelist,
            ]);
        }

        // no search term provided
        // create page with only search form
        return $this->render('search.html.twig', [
            'lang' => $_locale,
            'search_result' => null,
            'all_application_names' => $this->getAllApplicationsName($_locale, $translator),
            'search_form' => $this->createSearchForm()->createView(),
            'languages' => GeneratedData::languagelist,
            'branch_descriptions' => GeneratedData::branch_description,
        ]);

    }

    /**
     * @Route("/{_locale}/{module}", name="module", defaults={"_locale": "en"}, requirements={"_locale": "%app.locales%"})
     * @param string $_locale
     * @param string $module
     * @param TranslatorInterface $translator
     * @return Response
     * @throws \Exception
     */
    public function moduleAction(string $_locale, string $module, TranslatorInterface $translator): Response
    {
        if (!array_key_exists($module, GeneratedData::modules_programs)) {
            throw new \Exception("Module is unknown");
        }

        $submodules = [];
        $subsubmodules = []; // contains submodules with a bigger hierarchy

        // first add all submodules that aren't kcm
        foreach (GeneratedData::modules_programs[$module] as $submodule) {
            if (in_array($module . '/' . $submodule, StaticAppData::ignore_array)) {
                continue;
            }
            $array = explode('/', $submodule);
            if (count($array) === 1 && $array[0]) {
                if (array_key_exists($_locale, GeneratedData::programs_docs[$submodule])) {
                    $branch_available = GeneratedData::programs_docs[$submodule][$_locale];
                    $submodules[$submodule] = ["branch" => $branch_available];
                }
            }
        }

        // now add the others submodules
        foreach (GeneratedData::modules_programs[$module] as $submodule) {
            if (in_array($module . '/' . $submodule, StaticAppData::ignore_array)) {
                continue;
            }
            $array = explode('/', $submodule);
            if (count($array) !== 1 && array_key_exists($_locale, GeneratedData::programs_docs[$submodule])) {
                $branch_available = GeneratedData::programs_docs[$submodule][$_locale];
                $subsubmodules[$array[0]][$array[1]] = ["branch" => $branch_available];
            }
        }

        return $this->render('module.html.twig', [
            'lang' => $_locale,
            'module' => $module,
            'categories' => $submodules,
            'subsubmodules' => $subsubmodules,
            'branch_descriptions' => GeneratedData::branch_description,
            'search_form' => $this->createSearchForm()->createView(),
            'all_application_names' => $this->getAllApplicationsName($_locale, $translator),
            'languages' => GeneratedData::languagelist,
        ]);
    }

    public function languageListDropdown()
    {
        return $this->render(
            'language_dropdown.html.twig', [
                'languages' => GeneratedData::languagelist,
                'full_language_name' => StaticAppData::full_language_name,
            ]
        );
    }

    /**
     * @return SearchBarType
     */
    private function createSearchForm(): FormInterface
    {
        // create search form
        $search = new Search();
        $form = $this->createForm(SearchBarType::class, $search, [
            'action' => $this->generateUrl('search'),
        ]);
        return $form;
    }

    private function getAllApplicationsName(string $_locale, TranslatorInterface $translator): string
    {
        $cache = new FilesystemAdapter();
        $applicationsNameCache = $cache->getItem('applicationsName.' . $_locale);

        if (!$applicationsNameCache->isHit()) {
            $applicationsName = [];
            foreach (GeneratedData::modules_programs as $module => $moduleContent) {
                foreach ($moduleContent as $submodule) {
                    $array = explode('/', $submodule);
                    if (count($array) == 1) {
                        $applicationsName[] = $translator->trans($submodule, [], "modules");
                    } elseif (count($array) == 2) {
                        $applicationsName[] = $translator->trans($array[1], [], "modules");
                    }
                }
            }
            $applicationsNameString = '';
            foreach ($applicationsName as $name) {
                $applicationsNameString .= $name . ', ';
            }
            $applicationsNameCache->set($applicationsNameString);
            $applicationsNameCache->expiresAt(new \DateTime('tomorrow'));
            $cache->save($applicationsNameCache);
            return $applicationsNameString;
        } else {
            return $applicationsNameCache->get();
        }
    }


    /**
     * Check if translation exist
     * @param string $app
     * @param string $lang
     * @return bool
     */
    function language_for_application_exist(string $app, string $lang): bool
    {
        return (array_key_exists($lang, $this->programs_docs[$app]));
    }

    /**
     * Check if text exist for the specified branch and language
     * @param $app
     * @param $lang
     * @param $branch
     * @return string the branch
     */
    function branch_exist($app, $lang, $branch): string
    {
        // no translations for the specified application in the current language
        if (!$this->language_for_application_exist($app, $lang)) {
            return "";
        }
        // Handle old URL - compatibility with the old schema
        $compatibility_branches = array(
            "development" => array("trunk5", "trunk4"),
            "stable" => array("stable5", "stable4"),
        );
        // Search in compatibility branches first
        if (array_key_exists($branch, $compatibility_branches)) {
            foreach ($compatibility_branches[$branch] as $comp_branch) {
                # echo "Looking for $comp_branch into ".var_dump($this->programs_docs[$app][$lang]);
                if (in_array($comp_branch, $this->programs_docs[$app][$lang])) {
                    return $comp_branch;
                }
            }
        }
        // Normal branch
        if (in_array($branch, $this->programs_docs[$app][$lang])) {
            return $branch;
        }
        return "";
    }
}

