<?php

namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\SearchType;

/**
 * Class SearchBarType
 * Used in the navbar to create the form
 * @package App\Form
 */
class SearchBarType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder ->add('search_term', SearchType::class, [
            'translation_domain' => 'app',
        ]);
    }
}

