# Installation instructions

The paths are hard-coded and absolute so you will need to create a new user
called **docs** and some hard-coded directory.

```bash
useradd --create-home docs
sudo su docs # log as the new user
cd ~
# see https://techbase.kde.org/Development/Git/Configuration#URL_Renaming for kde: prefix
git clone kde:websites/docs-kde-org docs
```

Next is the apache configuration, you can find an example in `apache-docs.kde.org.conf`
for production use. For development you can use this one.

```
<VirtualHost *:80>
    ServerAdmin  webmaster@kde.org
    ServerName   docs.localhost

    DocumentRoot /srv/www/docs.kde.org/public

    ScriptAlias /docsearch/ /usr/lib/xapian-omega/bin/omega
    Alias /images/xapian-omega /usr/share/images/xapian-omega
    SetEnv OMEGA_CONFIG_FILE /var/www/docs.kde.org/omega/omega.conf

    ErrorLog /var/log/httpd/docs.kde.org-error.log
    CustomLog /var/log/httpd/docs.kde.org.log combined

    <Directory "/usr/lib/xapian-omega/bin/omega">
        Options +ExecCGI
        AllowOverride None
        # Order deny,allow
        Allow from all
        Require all granted
    </Directory>
    
    <Directory /var/www/docs-kde-org/public>
        AllowOverride All
        Require all granted
        FallbackResource /index.php
    </Directory>

    # uncomment the following lines if you install assets as symlinks
    # or run into problems when compiling LESS/Sass/CoffeeScript assets
    <Directory /var/www/docs-kde-org>
        Options FollowSymlinks
    </Directory>

    <Directory "/usr/lib/cgi-bin/omega">
        Options +ExecCGI
        AllowOverride None
        Order deny,allow
        Allow from all
        Require all granted
    </Directory>
</VirtualHost>
```

Let's generate some doc now. For development purpose we don't want to generate all the docs for all the language
so we can edit `/home/docs/docs/docgen_conf.ini` and uncomment some lines:

```
languages=en,ca
packages=calligra,frameworks,kde-workspace
```

To generate the doc, do:

```
cd /home/docs/docs && ./kdedocgen.py -r -g -s -l doclogconfig.ini -c docgen_conf.ini
cd /home/docs/docs && ./create_generated_used.php /home/docs/docs/work >/home/docs/website/generated_used.inc.php 2>/home/docs/logs/genused.log

# only needed if you want to develop the xapian stuff 
cd /home/docs/docs/search && nice -n 19 ./do_xapian_index.sh 

# updload to local server
rsync -azC --delete /home/docs/website/ /srv/www/docs.kde.org/html
```

