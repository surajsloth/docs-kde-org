#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
# Copyright 2015  Luigi Toscano <luigi.toscano@tiscali.it>
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) version 3, or any
# later version accepted by the membership of KDE e.V. (or its
# successor approved by the membership of KDE e.V.), which shall
# act as a proxy defined in Section 6 of version 3 of the license.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library.  If not, see <http://www.gnu.org/licenses/>.

from __future__ import absolute_import
import codecs
import logging
import os
import re
import shutil
import subprocess

import git

from .utils import find_files_pattern, file_replace_re, mkdirp


LOGGER = logging.getLogger(__name__)


class DocEnvironment(object):

    ENV_ID = ''

    def __init__(self, config):
        self.config = config
        mkdirp(self.config.workdir)

    def get_config(self, key):
        """Return the value for the specified key in the current environment.
        """
        return self.config.get_env_value(self.ENV_ID, key)

    def setup(self):
        LOGGER.info('Setting up environment %s' % (self.ENV_ID))

    def local_catalog_path(self, branch):
        """Additional local DocBook catalogs, if needed."""
        return None

    def customization_dir(self, branch):
        """Local working customization directory for the specified branch."""
        return os.path.join(self.config.op_branch_dir(branch),
                            'customization')

    def xslt_stylesheet_path(self, branch):
        """Path to the main XSL stylesheet."""
        raise NotImplementedError('This method must be redefined')

    def checkout_doc_init(self, branch):
        # copy prepared kdoctools files to the branch
        pass

    def checkout_doc_post(self, branch, languages=None, packages=None):
        pass

    def init_documentation_branch(self, branch):
        """Initialize the website directory of the specified branch."""
        pass

    def get_extended_env(self, branch):
        """Return the current environment extended with the environment
        variables requested by the XSL processor.
        """
        extra_catalog_path = self.local_catalog_path(branch)
        xslt_env = dict(os.environ)
        # FIXME: extend, not override, the variables
        if extra_catalog_path:
            xslt_env.update({'XML_CATALOG_FILES': extra_catalog_path,
                             'SGML_CATALOG_FILES': extra_catalog_path})
        return xslt_env

    def get_common_path(self, branch, lang):
        """Return the path to the common documentation resources."""
        raise NotImplementedError('This method must be redefined')

    def copy_image_files(self, docbookdir, branch, lang, pkg):
        """ Found available PNGs for the current language and - if missing -
        from English, and copy them."""
        webnext_dir = os.path.join(self.config.website_pkg_dir(branch, lang,
                                                               pkg,
                                                               next=True),
                                   docbookdir)
        docdir_rel = os.path.relpath(webnext_dir, self.config.nextwebsitedir)
        png_all_languages = [lang]
        if 'en' not in png_all_languages:
            png_all_languages.append('en')
        for png_lang in png_all_languages:
            src_img_dir = os.path.join(self.config.op_pkg_dir(branch,
                                                              png_lang, pkg),
                                       docbookdir)
            try:
                for png_file in os.listdir(src_img_dir):
                    if not png_file.endswith('.png'):
                        continue
                    if not os.path.isfile(os.path.join(webnext_dir, png_file)):
                        LOGGER.debug('copying %s from "%s" into %s' %
                                     (png_file, png_lang, docdir_rel))
                        shutil.copy2(os.path.join(src_img_dir, png_file),
                                     webnext_dir)
            except Exception as exc:
                LOGGER.warn('error while trying to access %s (%s)' %
                            (src_img_dir, str(exc)))

    def generate_doc_html(self, docbookdir, branch, lang, pkg):
        docdir = os.path.join(self.config.op_pkg_dir(branch, lang, pkg),
                              docbookdir)
        xslt_env = self.get_extended_env(branch)
        cmd_html = ['nice', '-n', '19', 'xsltproc', '--stringparam',
                    'kde.common', self.get_common_path(branch, lang),
                    self.xslt_stylesheet_path(branch),
                    os.path.join(docdir, 'index.docbook')]
        docdir_rel = os.path.relpath(docdir, self.config.workdir)
        LOGGER.debug('HTML generation (%s)' % (docdir_rel))
        webnext_dir = os.path.join(self.config.website_pkg_dir(branch, lang,
                                                               pkg,
                                                               next=True),
                                   docbookdir)
        mkdirp(webnext_dir)
        proc = subprocess.Popen(cmd_html, cwd=webnext_dir, env=xslt_env,
                                stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        proc_out, proc_err = proc.communicate()
        proc_rc = proc.returncode
        if proc_rc != 0:
            LOGGER.warning('Error running "%s" from "%s", catalog: %s:\n' %
                           (' '.join(cmd_html), webnext_dir,
                            self.local_catalog_path(branch)))
            proc_data = [('stdout', proc_out), ('stderr', proc_err)]
            for p_msg in proc_data:
                try:
                    LOGGER.warning('%s:\n%s' % (p_msg[0], p_msg[1]))
                except UnicodeDecodeError:
                    LOGGER.warning('%s:\n%s' % (p_msg[0], p_msg[1].decode(
                                                'latin-1')))

    def generate_doc_pdf(self, docbookdir, branch, lang, pkg):
        docdir = os.path.join(self.config.op_pkg_dir(branch, lang, pkg),
                              docbookdir)
        webnext_dir = os.path.join(self.config.website_pkg_dir(branch, lang,
                                                               pkg,
                                                               next=True),
                                   docbookdir)

        xslt_env = self.get_extended_env(branch)
        additional_env = {}
        # the following variables are needed by buildpdf.sh
        # DBLATEX_BASE_DIR must point to the base directory containing
        #  dblatex-cvs-install
        additional_env['DBLATEX_BASE_DIR'] = self.config.resource_dir
        # TEXINPUTS is extended with the next website directory, which
        #  contains the images referenced by the document
        texinputs = xslt_env.get('TEXINPUTS', '')
        if texinputs:
            texinputs = ':%s' % (texinputs)
        additional_env['TEXINPUTS'] = '%s%s' % (webnext_dir, texinputs)
        xslt_env.update(additional_env)
        cmd_pdf = ['nice', '-n', '19', os.path.join(self.config.resource_dir,
                                                    'buildpdf.sh'),
                   os.path.join(docdir, 'index.docbook')]
        docdir_rel = os.path.relpath(docdir, self.config.workdir)
        LOGGER.debug('PDF generation (%s)' % (docdir_rel))

        mkdirp(webnext_dir)
        proc = subprocess.Popen(cmd_pdf, cwd=webnext_dir, env=xslt_env,
                                stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        proc_out, proc_err = proc.communicate()
        proc_rc = proc.returncode
        if proc_rc != 0:
            # FIXME: investigate why the output from the command above (hence
            # pdftex) bails out here without using decode('latin-1')
            LOGGER.warning('Error running "%s" from "%s", catalog: %s, '
                           'additional env: %s' %
                           (' '.join(cmd_pdf), webnext_dir,
                            self.local_catalog_path(branch), additional_env))
            proc_data = [('stdout', proc_out), ('stderr', proc_err)]
            for p_msg in proc_data:
                try:
                    LOGGER.warning('%s:\n%s' % (p_msg[0], p_msg[1]))
                except UnicodeDecodeError:
                    LOGGER.warning('%s:\n%s' % (p_msg[0], p_msg[1].decode(
                                                'latin-1')))


#class KDELibs4Environment(DocEnvironment):
#
#    ENV_ID = 'kdelibs4'
#
#    def __init__(self, config):
#        super(KDELibs4Environment, self).__init__(config)
#        self._help_re = re.compile(r'help:/((kioslave/|kcontrol/)?[A-Za-z0-9_\-]+)/*')
#        self.kdelibs_dir = os.path.join(self.config.workdir, 'kdelibs_repo')
#        # FIXME: better check if it exists
#        self.kdelibs_make_dir = self.get_config('compile_dir')
#
#    def setup(self):
#        super(KDELibs4Environment, self).setup()
#        if not os.path.isdir(self.kdelibs_dir):
#            git.Repo.clone_from('git://anongit.kde.org/kdelibs',
#                                self.kdelibs_dir, branch='KDE/4.14')
#        else:
#            kdelibs_repo = git.Repo(self.kdelibs_dir)
#            b414 = kdelibs_repo.heads['KDE/4.14'].checkout()
#            pull_res = kdelibs_repo.remotes.origin.pull()
#
#        # Pass the location of the kdelibs4 repository to the shipped
#        # simplified Makefile
#        cmd_make = ['make', 'PATH_KDELIBS=%s' % (self.kdelibs_dir)]
#        proc = subprocess.Popen(cmd_make, cwd=self.kdelibs_make_dir,
#                                stdout=subprocess.PIPE, stderr=subprocess.PIPE)
#        proc_out, proc_err = proc.communicate()
#        proc_rc = proc.returncode
#        if proc_rc != 0:
#            LOGGER.warning('%s\n%s' % (proc_out, proc_err))
#
#    def local_catalog_path(self, branch):
#        """Additional local DocBook catalogs"""
#        catalog_path = os.path.join(self.customization_dir(branch),
#                                    'catalog.xml')
#        return catalog_path
#
#    def xslt_stylesheet_path(self, branch):
#        return os.path.join(self.customization_dir(branch),
#                            'kde-chunk-online.xsl')
#
#    def checkout_doc_init(self, branch):
#        super(KDELibs4Environment, self).checkout_doc_init(branch)
#        # copy prepared customization files to the branch
#        LOGGER.info('Initializing doc checkout for %s' % (branch))
#        # rm customization, copy the directory from kdelibs
#        LOGGER.debug('doc_init, %s: copying fresh customization dir' %
#                     (branch))
#        if os.path.isdir(self.customization_dir(branch)):
#            shutil.rmtree(self.customization_dir(branch))
#        bin_customization_dir = os.path.join(self.kdelibs_dir,
#                                             'kdoctools/customization')
#        shutil.copytree(bin_customization_dir, self.customization_dir(branch),
#                        symlinks=True)
#        # copy kde-*-online.xsl
#        LOGGER.debug('doc_init, %s: copying kde-*-online.xsl' % (branch))
#        for xsl_templ in ['kde-chunk-online.xsl', 'kde-navig-online.xsl']:
#            shutil.copy(xsl_templ, os.path.join(self.customization_dir(branch),
#                        xsl_templ))
#        # generate kdex.dtd
#        LOGGER.debug('doc_init, %s: generating kdex.dtd' % (branch))
#        with codecs.open(os.path.join(self.customization_dir(branch),
#                                      'dtd/kdex.dtd.cmake'), 'r',
#                         encoding='utf-8') as sf, \
#            codecs.open(os.path.join(self.customization_dir(branch),
#                                     'dtd/kdex.dtd'), 'w',
#                        encoding='utf-8') as df:
#            for line in sf:
#                df.write(line.replace('@DOCBOOKXML_CURRENTDTD_DIR@',
#                                      self.get_config('docbookxml_path')))
#        # generate kde-include-common.xsl
#        LOGGER.debug('doc_init, %s: generating kde-include-common.xsl' %
#                     (branch))
#        docbookxsl_path = self.get_config('docbookxsl_path')
#        with codecs.open(os.path.join(self.customization_dir(branch),
#                                      'kde-include-common.xsl.cmake'),
#                         'r', encoding='utf-8') as sf, \
#            codecs.open(os.path.join(self.customization_dir(branch),
#                                     'kde-include-common.xsl'), 'w',
#                        encoding='utf-8') as df:
#            for line in sf:
#                df.write(line.replace('@DOCBOOKXSL_DIR@', docbookxsl_path))
#        # execute docbookl10nhelper
#        LOGGER.debug('doc_init, %s: running docbookl10nhelper' % (branch))
#        xsl_doc_dir = os.path.join(self.customization_dir(branch), 'xsl')
#        docbookl10nhelper_cmd = [os.path.join(self.kdelibs_make_dir,
#                                              'docbookl10nhelper'),
#                                 docbookxsl_path, xsl_doc_dir, xsl_doc_dir]
#        proc = subprocess.Popen(docbookl10nhelper_cmd,
#                                stdout=subprocess.PIPE, stderr=subprocess.PIPE)
#        proc_out, proc_err = proc.communicate()
#        proc_rc = proc.returncode
#        if proc_rc != 0:
#            LOGGER.debug('%s\n%s' % (proc_out, proc_err))
#
#    def checkout_doc_post(self, branch, languages=None, packages=None):
#        pass
#
#    def init_documentation_branch(self, branch):
#        """Initialize the website directory of the specified branch."""
#        # rm common, copy it from kdelibs
#        LOGGER.debug('doc_init, %s: copying common dir' % (branch))
#        common_en = os.path.join(self.config.website_branch_dir(branch,
#                                                                next=True),
#                                 'common')
#        if os.path.isdir(common_en):
#            shutil.rmtree(common_en)
#        shutil.copytree(os.path.join(self.kdelibs_dir, 'doc/common'),
#                        common_en, symlinks=True)
#
#    def get_common_path(self, branch, lang):
#        """Return the path to the common documentation resources."""
#        return '/%s/common/' % (branch)
#
#    def generate_doc_html(self, docbookdir, branch, lang, pkg):
#        super(KDELibs4Environment, self).generate_doc_html(docbookdir, branch,
#                                                           lang, pkg)
#        nextweb_docdir = os.path.join(self.config.website_pkg_dir(
#            branch, lang, pkg, next=True), docbookdir)
#        # symlinks to "common" are needed only in KDELibs4Environment
#        # relative path to the global common directory
#        common_symlink_path = os.path.join(nextweb_docdir, 'common')
#        common_relpath = os.path.relpath(os.path.join(
#            self.config.website_branch_dir(branch, next=True), 'common'),
#            nextweb_docdir)
#        if os.path.exists(common_symlink_path):
#            if not os.path.islink(common_symlink_path):
#                LOGGER.warning('%s exists and it is not a symlink' %
#                               (common_symlink_path))
#        else:
#            dbg_location = os.path.relpath(nextweb_docdir,
#                                           self.config.website_branch_dir(
#                                               branch, next=True))
#            LOGGER.debug('adding symlink to "%s" common into: %s' % (lang,
#                         dbg_location))
#            os.symlink(common_relpath, common_symlink_path)
#
#        # replace the internal help:/... strings. This step should be done
#        # by XSLT
#        replaced_string = (r'/?branch=%s&language=%s&application=\1&path=' %
#                           (branch, lang))
#        for docdir_file in os.listdir(nextweb_docdir):
#            if docdir_file.endswith('.html'):
#                html_file_path = os.path.join(nextweb_docdir, docdir_file)
#                try:
#                    file_replace_re(html_file_path, self._help_re,
#                                    replaced_string)
#                except Exception as exc:
#                    LOGGER.warning('Error replacing %s: %s' % (html_file_path,
#                                                               str(exc)))
#

class KF5Environment(DocEnvironment):

    ENV_ID = 'kf5'

    def __init__(self, config):
        super(KF5Environment, self).__init__(config)
        self._help_re = re.compile(
            r'help:/((kioslave5?/|kcontrol5?/)?[A-Za-z0-9_\-]+)/*')
        lib_tmp_dir = os.path.join(self.config.workdir, 'kdoctools')
        self.kdoctools_src_dir = os.path.join(lib_tmp_dir, 'repo')
        self.kdoctools_build_dir = os.path.join(lib_tmp_dir, 'build')
        self.kdoctools_compiled_dir = os.path.join(lib_tmp_dir,
                                                   'kdoctools5bin')

    def setup(self):
        super(KF5Environment, self).setup()
        try:
            if not os.path.isdir(self.kdoctools_src_dir):
                kdoctools_repo = git.Repo.clone_from(
                    'git://anongit.kde.org/kdoctools', self.kdoctools_src_dir,
                    branch='master')
            else:
                kdoctools_repo = git.Repo(self.kdoctools_src_dir)
                master_branch = kdoctools_repo.heads['master'].checkout()
                pull_res = kdoctools_repo.remotes.origin.pull(*['--tags'])
            requested_ref = self.get_config('lib_ref')
            if requested_ref:
                # FIXME: error handling
                kdoctools_repo.head.reference = \
                    kdoctools_repo.refs[requested_ref]
                kdoctools_repo.head.reset(index=True, working_tree=True)
        except git.exc.GitCommandError as e:
            LOGGER.warn('Error while checking out the KF5 environment: %s' %
                        (str(e)))
        force_cleanup = False
        force_cleanup_str = self.get_config('clean_build')
        if force_cleanup_str and force_cleanup_str.lower() == 'yes':
            force_cleanup = True

        if force_cleanup:
            if os.path.isdir(self.kdoctools_build_dir):
                shutil.rmtree(self.kdoctools_build_dir)
        if not os.path.isdir(self.kdoctools_build_dir):
            mkdirp(self.kdoctools_build_dir)
            force_cleanup = True

        cmd_cmake = (['cmake', '-DCMAKE_INSTALL_PREFIX=%s ' %
                      (self.kdoctools_compiled_dir),
                      '-DCMAKE_BUILD_TYPE=debugfull',
                      '-DMEINPROC_NO_KARCHIVE=ON',
                      '%s' % (self.kdoctools_src_dir)])
        cmd_make = 'make'
        cmds = [cmd_make]
        if force_cleanup:
            # rerun cmake only for cleanup/newly created build directory
            cmds.insert(0, cmd_cmake)

        for cmd in cmds:
            proc = subprocess.Popen(cmd, cwd=self.kdoctools_build_dir,
                                    stdout=subprocess.PIPE,
                                    stderr=subprocess.PIPE)
            proc_out, proc_err = proc.communicate()
            proc_rc = proc.returncode
            if proc_rc != 0:
                LOGGER.warning('%s\n%s\n%s' % (' '.join(cmd), proc_out,
                                               proc_err))
                if not os.path.isdir(self.kdoctools_compiled_dir):
                    # bail out, no previously working kdoctools
                    raise Exception('Setup error')
                    # otherwise return, a new kdoctools can not be installed;
                    # the old one is reused
                return
        # cmake && make worked, so we can (safely) remove the old compiled
        # directory and replace with the up-to-date content
        if os.path.isdir(self.kdoctools_compiled_dir):
            shutil.rmtree(self.kdoctools_compiled_dir)
        cmd_makeinstall = ['make', 'install']
        proc = subprocess.Popen(cmd_makeinstall, cwd=self.kdoctools_build_dir,
                                stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        proc_out, proc_err = proc.communicate()
        proc_rc = proc.returncode
        if proc_rc != 0:
            LOGGER.warning('%s\n%s' % (proc_out, proc_err))

    def local_catalog_path(self, branch):
        """Additional local DocBook catalogs"""
        return os.path.join(self.customization_dir(branch), 'catalog.xml')

    def xslt_stylesheet_path(self, branch):
        return os.path.join(self.customization_dir(branch),
                            'kde-chunk-online.xsl')

    def checkout_doc_init(self, branch):
        super(KF5Environment, self).checkout_doc_init(branch)
        # copy prepared customization files to the branch
        LOGGER.info('Initializing doc checkout for %s' % (branch))
        # rm customization, copy the directory from kdelibs
        LOGGER.debug('doc_init, %s: copying fresh customization dir' %
                     (branch))
        if os.path.isdir(self.customization_dir(branch)):
            shutil.rmtree(self.customization_dir(branch))
        bin_customization_dir = os.path.join(self.kdoctools_compiled_dir,
                                             'share/kf5/kdoctools/'
                                             'customization')
        shutil.copytree(bin_customization_dir, self.customization_dir(branch),
                        symlinks=True)
        # copy kde-*-online.xsl
        LOGGER.debug('doc_init, %s: copying kde-*-online.xsl' % (branch))
        for xsl_templ in ['kde-chunk-online.xsl', 'kde-navig-online.xsl']:
            shutil.copy(xsl_templ, os.path.join(self.customization_dir(branch),
                        xsl_templ))

    def init_documentation_branch(self, branch):
        """Initialize the website directory of the specified branch."""
        # rm kdoctools5-common, copy it from kdoctools
        # Warning: only English for now
        LOGGER.debug('doc_init, %s: copying kdoctools5-common dir' % (branch))
        common_en = os.path.join(self.config.website_lang_dir(branch, 'en',
                                                              next=True),
                                 'kdoctools5-common')
        if os.path.isdir(common_en):
            shutil.rmtree(common_en)
        shutil.copytree(os.path.join(self.kdoctools_compiled_dir,
                                     'share/doc/HTML/en/kdoctools5-common/'),
                        common_en, symlinks=True)

    def get_common_path(self, branch, lang):
        """Return the path to the common documentation resources.
        Warning: the language is not used, only English resources are
        used for now."""
        return '/%s/en/kdoctools5-common/' % (branch)

    def generate_doc_html(self, docbookdir, branch, lang, pkg):
        super(KF5Environment, self).generate_doc_html(docbookdir, branch,
                                                      lang, pkg)
        nextweb_docdir = os.path.join(self.config.website_pkg_dir(branch,
                                                                  lang, pkg,
                                                                  next=True),
                                      docbookdir)
        # replace the internal help:/... strings. This step should be done
        # by XSLT
        replaced_string = (r'/?branch=%s&language=%s&application=\1&path=' %
                           (branch, lang))
        for docdir_file in os.listdir(nextweb_docdir):
            if docdir_file.endswith('.html'):
                html_file_path = os.path.join(nextweb_docdir, docdir_file)
                try:
                    file_replace_re(html_file_path, self._help_re,
                                    replaced_string)
                except Exception as exc:
                    LOGGER.warning('Error replacing %s: %s' % (html_file_path,
                                                               str(exc)))


class DocEnvironmentsManager(object):
    """Read the list of requested environments from the configuration files
    and generate them."""
    def __init__(self, config):
        self.config = config
        self._envs = {}
        self._init_envs()

    def _init_envs(self):
        """Initialize all the environment in configuration."""
        for configured_env in self.config.configured_environments:
            """Find the class which implements the requested env and
            dynamically creates it.
            """
            env_class = next((e_c for e_c in DocEnvironment.__subclasses__()
                              if e_c.ENV_ID == configured_env), None)
            print(configured_env)
            self._envs[configured_env] = env_class(self.config)

    def setup_all(self):
        for env in self._envs.values():
            env.setup()

    def get_env(self, branch):
        """Return the environment object for the specified branch, based on
        the branch configuration."""
        if branch not in self.config.enabled_branches:
            return None
        env_name = self.config.get_branch_value(branch, 'env')
        if not env_name:
            return None
        return self._envs[env_name]
