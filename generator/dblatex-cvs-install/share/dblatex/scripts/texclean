#!/usr/bin/env perl
    eval 'exec /usr/bin/env perl -S $0 ${1+"$@"}'
        if $running_under_some_shell;

use File::Basename;
use Getopt::Std;

#
# Formattage des tableaux, pour g�rer les cellules sur plusieurs lignes
# use table;
#
$row = "";
@mrows = ();
@sizes = ();
@aligs = ();
$nbcols = 0;

$force_hyphen = 1;
$istable = 0;
$tblparse = 0;

#
# Table header parsed to know the column sizes and alignments
#
sub prepare_table
{
  local($line) = $_[0];
  my $i = 0;
  my @a = ();
  
  @a = split(/(p\{[^}]*})/, $line);
  foreach (@a) {
    if (/p\{/) {
      ($sizes[$i] = $_) =~ s/p\{(.*)}/$1/;
      $i++;
    } else {
      ($aligs[$i] = $_) =~ s/.*{(.[^}]*)}$/$1/;
    }
  }
  $nbcols = $i;
}

sub hline2cline
{
  local($row) = $_[0];
  local($ncols) = $_[1];
  my $i;
  my $first = 1;
  my $last = 1;
  my $pl = "";
  my $bord = 0;
  my $prow = "";
  my $erow = "";

  my @u;
  $row =~ s/\\hline/<hline>/;
  $row =~ s/\\tabularnewline/<tabularnewline>/;
  @u = split("<hline>", $row);
  if ($#u > 0) {
    $prow = $u[0];
    $prow .= "\\hline";
    $row = $u[1];
  }
  @u = split("<tabularnewline>", $row);
  if ($#u > 0) {
    $row = $u[0];
    $erow = "\\tabularnewline";
    $erow .= $u[1];
  }

  # dump_mrows($ncols);
  for($i = 0; $i < $ncols; $i++) {
    # print $to "mr($i)=".$mrows[$i];
    if ($mrows[$i] <= 0) {
      # on m�morise la ligne � tracer
      $bord = 1;
      $last = $i+1;
    } else {
      # on trace la ligne jusqu'� cette cellule
      if ($bord) {
        $pl = "$pl\\cline{$first-$last}";
        $bord = 0;
      }
      $first = $i+2;
    }
  }
  if ($bord && (($first != 1) || ($last != $ncols))) {
    $pl = "$pl\\cline{$first-$last}";
  }
  if ($pl ne "") {
    $prow =~ s/\\hline/$pl/;
  }
  return ($prow, $row, $erow);
}

#
# Main parsing table routine. The principle is to process each table row
# when rows are complete (a row can cover several lines).
#
sub table_parse
{
  local($line) = $_[0];
  local($to) = $_[1];
  my $i, $pos;
  my @columns = ();

  if (/%%% parse_table/) {
    # note that the following table will need to be parsed
    $tblparse = 1;
    # print "Parsing table\n";
  }
  if (/begin\{longtable}/) {
    $istable = 1;
    if ($tblparse) {
      prepare_table($_);
    }
  }
  if (not($istable)) {
    print $to $line;
    return;
  }
  if (/end\{longtable}/) {
    #
    # table ends, flush the current row
    #
    print $to "$row$line";
    $istable = 0;
    $tblparse = 0;
    $row = "";
    $nbcols = 0;
    return;
  }
  # if no parsing, just print the line
  if (not($tblparse)) {
    print $to $line;
    return;
  }
  
  #
  # when parsing, only process a complete row
  #
  $row = "$row$line";
  if (not(/\\tabularnewline/)) {
    return;
  }
  #
  # for the previous row, print borders (depends on pending multirow cells)
  #
  ($brow, $row, $erow) = hline2cline($row, $nbcols);
  @columns = split('&', $row);
  $nbcols = ($#columns > $nbcols) ? $#columns : $nbcols;

  #
  # for each column, for the current row:
  # - insert the empty cells due to pending mrow cells,
  # - update the pending mrow cells count,
  # - setup the new pending mrow cells,
  # - print the "multirow" cells
  #
  print $to $brow;

  for ($i=0, $pos=0; $pos<$nbcols; $pos++) {
    $c = $columns[$i];

    if ($mrows[$pos] > 0) {
      #
      # insert an empty cell, and update the pending mrow cells for this
      # column.
      #
      print $to " & ";
      $mrows[$pos] --;
    } elsif ($i > $#columns) {
      # print the missing ending cells
      print $to " & ";
    } else {
      # shift to the real position
      if ($c =~ /%<num=/) {
        $rpos = $c;
        $rpos =~ s/[^%]*%<num=([^>]*).*\n/$1/;
        $rpos -= 1;
        # skip the info
        $c =~ s/%<num=.*>%\n//;
        while ($rpos > $pos) {
          print $to " & ";
          $pos++;
          if ($mrows[$pos] > 0) {
            $mrows[$pos] --;
          }
        }
      }
      if ($c =~ /multirow/) {
        # set the pending mrows cells count
        $mrows[$pos] = $c;
        $mrows[$pos] =~ s/\n//g;
        $mrows[$pos] =~ s/.*multirow\{([^}]*)}.*/$1/;
        $mrows[$pos] -= 1;
        #
        # set the column size and its alignment (which is lost by
        # multirow that aligns to left by default)
        #
        $s = $sizes[$pos];
        $a = $aligs[$pos];
        $c =~ s/\*\{/{$s}{$a /;
        # macro to use instead of multirow, to be compatible with \tsize
        $c =~ s/multirow/mrow/;
      }
      # shift the multicolums
      if ($c =~ /multicolumn/) {
        $mcols = $c;
        $mcols =~ s/\n//g;
        $mcols =~ s/.*multicolumn\{([^}]*)}.*/$1/g;
        $pos += ($mcols - 1);
      }
      print $to $c;
      if ($i < $#columns) {
        # print the separator
        print $to " & ";
      }
      $i ++;
    }
  }
  print $to $erow;
  $row = "";
}

$keyon = '\\\\xt';
$keyoff = '/xt';

# Known embedded commands
$do_hyphen = 0;

sub command_update
{
  local($line) = $_[0];
  my $cmd;

  if ($line =~ /%% texclean\(/) {
    ($cmd = $line) =~ s/.*texclean\(([^\)]*)\).*\n/$1/;
    if ($cmd eq "hyphenon") {
      $do_hyphen = 1;
    } elsif ($cmd eq "hyphenoff") {
      $do_hyphen = 0;
    }
  }
}

sub translate
{
  local($line) = $_[0];
  my $hyphenize = 0;

  if (($istable && $force_hyphen) || $do_hyphen) {
    $hyphenize = 1;
  }
  if ($hyphenize) {
    # Two passes, to cut every letter (not every two letters)
    $line =~ s/([^ 0-9~&#\t])([^ 0-9~&#\t\n])/$1<cut>$2/g;
    $line =~ s/<cut>([^ 0-9~&#\t])([^ 0-9~&#\t\n])/<cut>$1<cut>$2/g;
  }
  $line =~ s/^[\s\n]*$/ /g;
  $line =~ s/\\\\\ \ \ /dbackslash/g;
  $line =~ s/\\\\/gotothenextline/g;
  $line =~ s/\\/\\textbackslash/g;
  $line =~ s/_/\\_/g;
  $line =~ s/{/\\{/g;
  $line =~ s/}/\\}/g;
  $line =~ s/~/\\texttildelow{}/g;
  $line =~ s/%/\\%/g;
  $line =~ s/\^/\\\^{}/g;
  $line =~ s/\$/\\\$/g;
  $line =~ s/\"/\\textacutedbl{}/g;
  $line =~ s/&#1072;&#770;/\\\^{\\cyra}/g;
  $line =~ s/&#1072;&#785;/\\textroundcap{\\cyra}/g;
  $line =~ s/&#1077;&#785;/\\textroundcap{\\cyre}/g;
  $line =~ s/&#1086;&#785;/\\textroundcap{\\cyro}/g;
  $line =~ s/&#1110;&#785;/\\textroundcap{\\cyrii}/g;
  $line =~ s/&#260;/\\k{A}/g;
  $line =~ s/&#261;/\\k{a}/g;
  $line =~ s/&#262;/\\'{C}/g;
  $line =~ s/&#263;/\\'{c}/g;
  $line =~ s/&#268;/\\v{C}/g;
  $line =~ s/&#269;/\\v{c}/g;
  $line =~ s/&#270;/\\v{D}/g;
  $line =~ s/&#271;/\\v{d}/g;
  $line =~ s/&#278;/\\.{E}/g;
  $line =~ s/&#279;/\\.{e}/g;
  $line =~ s/&#280;/\\k{E}/g;
  $line =~ s/&#281;/\\k{e}/g;
  $line =~ s/&#282;/\\v{E}/g;
  $line =~ s/&#283;/\\v{e}/g;
  $line =~ s/&#286;/\\u{G}/g;
  $line =~ s/&#287;/\\u{g}/g;
  $line =~ s/&#302;/\\k{I}/g;
  $line =~ s/&#303;/\\k{i}/g;
  $line =~ s/&#304;/\\.{I}/g;
  $line =~ s/&#305;/\\i{}/g;
  $line =~ s/&#321;/\\L{}/g;
  $line =~ s/&#322;/\\l{}/g;
  $line =~ s/&#323;/\\'{N}/g;
  $line =~ s/&#324;/\\'{n}/g;
  $line =~ s/&#327;/\\v{N}/g;
  $line =~ s/&#328;/\\v{n}/g;
  $line =~ s/&#336;/\\H{O}/g;
  $line =~ s/&#337;/\\H{o}/g;
  $line =~ s/&#339;/\\oe{}/g;
  $line =~ s/&#344;/\\v{R}/g;
  $line =~ s/&#345;/\\v{r}/g;
  $line =~ s/&#346;/\\'{S}/g;
  $line =~ s/&#347;/\\'{s}/g;
  $line =~ s/&#350;/\\c{S}/g;
  $line =~ s/&#351;/\\c{s}/g;
  $line =~ s/&#352;/\\v{S}/g;
  $line =~ s/&#353;/\\v{s}/g;
  $line =~ s/&#355;/\\c{t}/g;
  $line =~ s/&#356;/\\v{T}/g;
  $line =~ s/&#357;/\\v{t}/g;
  $line =~ s/&#362;/\\={U}/g;
  $line =~ s/&#363;/\\={u}/g;
  $line =~ s/&#367;/\\r{u}/g;
  $line =~ s/&#368;/\\H{U}/g;
  $line =~ s/&#369;/\\H{u}/g;
  $line =~ s/&#370;/\\k{U}/g;
  $line =~ s/&#371;/\\k{u}/g;
  $line =~ s/&#377;/\\'{Z}/g;
  $line =~ s/&#378;/\\'{z}/g;
  $line =~ s/&#379;/\\.{Z}/g;
  $line =~ s/&#380;/\\.{z}/g;
  $line =~ s/&#381;/\\v{Z}/g;
  $line =~ s/&#382;/\\v{z}/g;
  $line =~ s/&#696;/\$\^\\textrm{y}\$/g;
  $line =~ s/&#700;/\'/g;
  $line =~ s/&#732;/\\textasciitilde{}/g;
  $line =~ s/&#902;/\\textgreek{'A}/g;
  $line =~ s/&#903;/\\textgreek{;}/g;
  $line =~ s/&#904;/\\textgreek{'E}/g;
  $line =~ s/&#905;/\\textgreek{'H}/g;
  $line =~ s/&#906;/\\textgreek{'I}/g;
  $line =~ s/&#908;/\\textgreek{'O}/g;
  $line =~ s/&#912;/\\textgreek{\\char242}/g;
  $line =~ s/&#913;/\\textAlpha{}/g;
  $line =~ s/&#914;/\\textBeta{}/g;
  $line =~ s/&#915;/\\textGamma{}/g;
  $line =~ s/&#916;/\\textDelta{}/g;
  $line =~ s/&#917;/\\textEpsilon{}/g;
  $line =~ s/&#918;/\\textZeta{}/g;
  $line =~ s/&#919;/\\textEta{}/g;
  $line =~ s/&#920;/\\textTheta{}/g;
  $line =~ s/&#921;/\\textIota{}/g;
  $line =~ s/&#922;/\\textKappa{}/g;
  $line =~ s/&#923;/\\textLambda{}/g;
  $line =~ s/&#924;/\\textMu{}/g;
  $line =~ s/&#925;/\\textNu{}/g;
  $line =~ s/&#926;/\\textXi{}/g;
  $line =~ s/&#927;/\\textOmikron{}/g;
  $line =~ s/&#928;/\\textPi{}/g;
  $line =~ s/&#929;/\\textRho{}/g;
  $line =~ s/&#931;/\\textSigma{}/g;
  $line =~ s/&#932;/\\textTau{}/g;
  $line =~ s/&#933;/\\textUpsilon{}/g;
  $line =~ s/&#934;/\\textPhi{}/g;
  $line =~ s/&#935;/\\textChi{}/g;
  $line =~ s/&#936;/\\textPsi{}/g;
  $line =~ s/&#937;/\\textOmega{}/g;
  $line =~ s/&#938;/\\textgreek{a}/g;
  $line =~ s/&#939;/\\textgreek{b}/g;
  $line =~ s/&#940;/\\textgreek{'a}/g;
  $line =~ s/&#941;/\\textgreek{'e}/g;
  $line =~ s/&#942;/\\textgreek{'h}/g;
  $line =~ s/&#943;/\\textgreek{'i}/g;
  $line =~ s/&#944;/\\textgreek{'"u}/g;
  $line =~ s/&#945;/\\textalpha{}/g;
  $line =~ s/&#946;/\\textbeta{}/g;
  $line =~ s/&#947;/\\textgamma{}/g;
  $line =~ s/&#948;/\\textdelta{}/g;
  $line =~ s/&#949;/\\textepsilon{}/g;
  $line =~ s/&#950;/\\textzeta{}/g;
  $line =~ s/&#951;/\\texteta{}/g;
  $line =~ s/&#952;/\\texttheta{}/g;
  $line =~ s/&#953;/\\textiota{}/g;
  $line =~ s/&#954;/\\textkappa{}/g;
  $line =~ s/&#955;/\\textlambda{}/g;
  $line =~ s/&#956;/\\textmugreek{}/g;
  $line =~ s/&#957;/\\textnu{}/g;
  $line =~ s/&#958;/\\textxi{}/g;
  $line =~ s/&#959;/\\textomikron{}/g;
  $line =~ s/&#960;/\\textpi{}/g;
  $line =~ s/&#961;/\\textrho{}/g;
  $line =~ s/&#962;/\\textvarsigma{}/g;
  $line =~ s/&#963;/\\textsigma{}/g;
  $line =~ s/&#964;/\\texttau{}/g;
  $line =~ s/&#965;/\\textupsilon{}/g;
  $line =~ s/&#966;/\\textphi{}/g;
  $line =~ s/&#967;/\\textchi{}/g;
  $line =~ s/&#968;/\\textpsi{}/g;
  $line =~ s/&#969;/\\textomega{}/g;
  $line =~ s/&#970;/\\textgreek{"i}/g;
  $line =~ s/&#971;/\\textgreek{"u}/g;
  $line =~ s/&#972;/\\textgreek{'o}/g;
  $line =~ s/&#973;/\\textgreek{'u}/g;
  $line =~ s/&#974;/\\textgreek{'w}/g;
  $line =~ s/&#1024;/\\`{\\CYRE}/g;
  $line =~ s/&#1025;/\\CYRYO{}/g;
  $line =~ s/&#1026;/\\CYRDJE{}/g;
  $line =~ s/&#1027;/\\'{\\CYRG}/g;
  $line =~ s/&#1028;/\\CYRIE{}/g;
  $line =~ s/&#1029;/S{}/g;
  $line =~ s/&#1030;/\\CYRII{}/g;
  $line =~ s/&#1031;/\\CYRYI{}/g;
  $line =~ s/&#1032;/\\CYRJE{}/g;
  $line =~ s/&#1033;/\\CYRLJE{}/g;
  $line =~ s/&#1034;/\\CYRNJE{}/g;
  $line =~ s/&#1035;/\\CYRTSHE{}/g;
  $line =~ s/&#1036;/\\'{\\CYRK}/g;
  $line =~ s/&#1037;/\\'{\\CYRI}/g;
  $line =~ s/&#1038;/\\CYRUSHRT{}/g;
  $line =~ s/&#1039;/\\CYRDZHE{}/g;
  $line =~ s/&#1040;/\\CYRA{}/g;
  $line =~ s/&#1041;/\\CYRB{}/g;
  $line =~ s/&#1042;/\\CYRV{}/g;
  $line =~ s/&#1043;/\\CYRG{}/g;
  $line =~ s/&#1044;/\\CYRD{}/g;
  $line =~ s/&#1045;/\\CYRE{}/g;
  $line =~ s/&#1046;/\\CYRZH{}/g;
  $line =~ s/&#1047;/\\CYRZ{}/g;
  $line =~ s/&#1048;/\\CYRI{}/g;
  $line =~ s/&#1049;/\\CYRISHRT{}/g;
  $line =~ s/&#1050;/\\CYRK{}/g;
  $line =~ s/&#1051;/\\CYRL{}/g;
  $line =~ s/&#1052;/\\CYRM{}/g;
  $line =~ s/&#1053;/\\CYRN{}/g;
  $line =~ s/&#1054;/\\CYRO{}/g;
  $line =~ s/&#1055;/\\CYRP{}/g;
  $line =~ s/&#1056;/\\CYRR{}/g;
  $line =~ s/&#1057;/\\CYRS{}/g;
  $line =~ s/&#1058;/\\CYRT{}/g;
  $line =~ s/&#1059;/\\CYRU{}/g;
  $line =~ s/&#1060;/\\CYRF{}/g;
  $line =~ s/&#1061;/\\CYRH{}/g;
  $line =~ s/&#1062;/\\CYRC{}/g;
  $line =~ s/&#1063;/\\CYRCH{}/g;
  $line =~ s/&#1064;/\\CYRSH{}/g;
  $line =~ s/&#1065;/\\CYRSHCH{}/g;
  $line =~ s/&#1066;/\\CYRHRDSN{}/g;
  $line =~ s/&#1067;/\\CYRERY{}/g;
  $line =~ s/&#1068;/\\CYRSFTSN{}/g;
  $line =~ s/&#1069;/\\CYREREV{}/g;
  $line =~ s/&#1070;/\\CYRYU{}/g;
  $line =~ s/&#1071;/\\CYRYA{}/g;
  $line =~ s/&#1072;/\\cyra{}/g;
  $line =~ s/&#1073;/\\cyrb{}/g;
  $line =~ s/&#1074;/\\cyrv{}/g;
  $line =~ s/&#1075;/\\cyrg{}/g;
  $line =~ s/&#1076;/\\cyrd{}/g;
  $line =~ s/&#1077;/\\cyre{}/g;
  $line =~ s/&#1078;/\\cyrzh{}/g;
  $line =~ s/&#1079;/\\cyrz{}/g;
  $line =~ s/&#1080;/\\cyri{}/g;
  $line =~ s/&#1081;/\\cyrishrt{}/g;
  $line =~ s/&#1082;/\\cyrk{}/g;
  $line =~ s/&#1083;/\\cyrl{}/g;
  $line =~ s/&#1084;/\\cyrm{}/g;
  $line =~ s/&#1085;/\\cyrn{}/g;
  $line =~ s/&#1086;/\\cyro{}/g;
  $line =~ s/&#1087;/\\cyrp{}/g;
  $line =~ s/&#1088;/\\cyrr{}/g;
  $line =~ s/&#1089;/\\cyrs{}/g;
  $line =~ s/&#1090;/\\cyrt{}/g;
  $line =~ s/&#1091;/\\cyru{}/g;
  $line =~ s/&#1092;/\\cyrf{}/g;
  $line =~ s/&#1093;/\\cyrh{}/g;
  $line =~ s/&#1094;/\\cyrc{}/g;
  $line =~ s/&#1095;/\\cyrch{}/g;
  $line =~ s/&#1096;/\\cyrsh{}/g;
  $line =~ s/&#1097;/\\cyrshch{}/g;
  $line =~ s/&#1098;/\\cyrhrdsn{}/g;
  $line =~ s/&#1099;/\\cyrery{}/g;
  $line =~ s/&#1100;/\\cyrsftsn{}/g;
  $line =~ s/&#1101;/\\cyrerev{}/g;
  $line =~ s/&#1102;/\\cyryu{}/g;
  $line =~ s/&#1103;/\\cyrya{}/g;
  $line =~ s/&#1104;/\\`{\\cyre}/g;
  $line =~ s/&#1105;/\\cyryo{}/g;
  $line =~ s/&#1106;/\\cyrdje{}/g;
  $line =~ s/&#1107;/\\'{cyrg}/g;
  $line =~ s/&#1108;/\\cyrie{}/g;
  $line =~ s/&#1109;/s{}/g;
  $line =~ s/&#1110;/\\cyrii{}/g;
  $line =~ s/&#1111;/\\cyryi{}/g;
  $line =~ s/&#1112;/\\cyrje{}/g;
  $line =~ s/&#1113;/\\cyrlje{}/g;
  $line =~ s/&#1114;/\\cyrnje{}/g;
  $line =~ s/&#1115;/\\cyrtshe{}/g;
  $line =~ s/&#1116;/\\'{cyrk}/g;
  $line =~ s/&#1117;/\\`{cyri}/g;
  $line =~ s/&#1118;/\\cyrushrt{}/g;
  $line =~ s/&#1119;/\\cyrdzhe{}/g;
  $line =~ s/&#1168;/\\CYRGUP{}/g;
  $line =~ s/&#1169;/\\cyrgup{}/g;
  $line =~ s/&#4660;/ju\\'{e}/g;
  $line =~ s/&#8209;/-/g;
  $line =~ s/&#8211;/\\textendash{}/g;
  $line =~ s/&#8212;/\\textemdash{}/g;
  $line =~ s/&#8216;/`/g;  
  $line =~ s/&#8217;/\'/g;
  $line =~ s/&#8220;/{}``/g;
  $line =~ s/&#8221;/{}''/g;
  $line =~ s/&#8222;/\\quotedblbase{}/g;
  $line =~ s/&#8364;/\\texteuro{}/g;
  $line =~ s/&#8731;/\$\\sqrt\[3\]{}\$/g;
  $line =~ s/&#8734;/\$\\infty{}\$/g;
  $line =~ s/&#8747;/\$\\int{}\$/g;
  $line =~ s/&#8901;/\\textperiodcentered{}/g;
  $line =~ s/&#x2DC;/\\textasciitilde{}/g;
  $line =~ s/&#x2013;/\\textendash{}/g;
  $line =~ s/&#x2014;/\\textemdash{}/g;
  $line =~ s/&#x201C;/{}``/g;
  $line =~ s/&#x201D;/{}''/g;
  $line =~ s/&#8592;/\$\\leftarrow\$/g;
  $line =~ s/&#8593;/\$\\uparrow\$/g;
  $line =~ s/&#8594;/\$\\rightarrow\$/g;
  $line =~ s/&#8595;/\$\\downarrow\$/g;
  $line =~ s/&#8722;/\$-\$/g;
  $line =~ s/&#8730;/\$\\sqrt{}\$/g;
  $line =~ s/&#8800;/\$\\neq\$/g;
  $line =~ s/&#8804;/\$\\leq\$/g;
  $line =~ s/&#8805;/\$\\geq\$/g;
  $line =~ s/&#8968;/\$\\lceil\$/g;
  $line =~ s/&#8969;/\$\\rceil\$/g;
  $line =~ s/&#8970;/\$\\lfloor\$/g;
  $line =~ s/&#8971;/\$\\rfloor\$/g;
  $line =~ s/&#9472;/{}---{}\$/g;
  $line =~ s/&#9646;/\\rule{.5em}{10pt}/g;
  $line =~ s/&#10004;/\\checkmark{}/g;
  $line =~ s/&#10025;/\$\\star\$/g;
  $line =~ s/&#10680;/\$\\oslash\$/g;
  $line =~ s/&#10799;/\$\\times\$/g;
  $line =~ s/&#12409;/\ narabe\ /g;
  $line =~ s/&#20006;/ku/g;
  $line =~ s/&#20116;/go/g;
  $line =~ s/&#23559;/ji\\={a}ng/g;
  $line =~ s/&#30446;/mo/g;
  $line =~ s/&#40635;/m\\'{a}/g;
  $line =~ s/&#64256;/ff/g;
  $line =~ s/&#64257;/fi/g;
  $line =~ s/&#64258;/fl/g;
  $line =~ s/&#64259;/ffi/g;
  $line =~ s/&#64260;/ffl/g;
  $line =~ s/&#64261;/ft/g;
  $line =~ s/&/\\&/g;
  $line =~ s/#/\\#/g;
  $line =~ s/�/\$\\mu\$/g;
  $line =~ s/�/\\'{a}/g;
  $line =~ s/\240/~/g;
  $line =~ s/\\textbackslash/\\textbackslash{}/g;
  $line =~ s/\327/\$\\times\$/g;
  $line =~ s/�/\\ensuremath{�}/g;
  $line =~ s/-/-{}/g;
  $line =~ s/�/\$\\pm\$/g;
  $line =~ s/�/\\texttwosuperior{}/g;
  $line =~ s/�/\\textthreesuperior{}/g;
  $line =~ s/�/\$\\div\$/g;
  $line =~ s/�/\\pounds{}/g;
  $line =~ s/�/\\textyen{}/g;
  $line =~ s/\ >\ /\ \$>\$\ /g;
  $line =~ s/\ <\ /\ \$<\$\ /g;
#  $line =~ s/>>/\$\\gg\$/g;
#  $line =~ s/<</\$\\ll\$/g;
  $line =~ s/gotothenextline/\\\\/g;
  $line =~ s/dbackslash/\\textbackslash\\textbackslash/g;
  $line =~ s/placeholderforthespace/~/g;

  # force hyphenation if asked
  if ($hyphenize) {
    $line =~ s/<cut>/\\-/g;
  }
  return $line;
}

$figcount = 0;
%figdone = ();

sub eps2xxx
{
  local($in) = $_[0];
  local($out) = $_[1];
  local($format) = $_[2];

  my $action = "";
  for ($format) { 
    /pdf/ && do { $action = "epstopdf --outfile=$out $in"; last; };
    /png/ && do { $action = "convert $in $out"; last; };
  }
  return $action;
}

sub gif2xxx
{
  local($in) = $_[0];
  local($out) = $_[1];
  local($format) = $_[2];

  my $action = "convert $in $out";
  return $action;
}

sub scanformat
{
  my $f = $_[0];
  my @formats = ();
  my $ext = "";

  # Is there a suffix?
  ($file,$p,$ext) = fileparse($f, '\..*');

  # The prefered format depends on the expected output
  if ($figout eq "eps") {
    @formats = (".eps", ".fig", ".pdf", ".png", "gif", "");
  } else {
    @formats = (".pdf", ".png", ".eps", "gif", ".fig", "");
  }

  if ($ext ne "") {
    $fig = $f;
    if (not(-f $fig))  {
      $fig = "$path/$fig";
    }
  } else {
    print "Fig format scanning... ";
    # Look for the missing format
    LOOKUP: {
      foreach $e (@formats) {
        $ext = $e;
        if (-f "$f$ext") {
          $fig = "$f$ext";
          last LOOKUP;
        } elsif (-f "$path/$f$ext") {
          $fig = "$path/$f$ext";
          last LOOKUP;
        }
      }
    }
    print "found $ext\n";
  }
  $ext =~ s/\.//;
  return ($fig, $ext);
}

sub figconvert
{
  local($line) = $_[0];
  my $dofig = "";
  my $figcmd = "";

  # Is there a graphic included here
  if (/\\includegraphics[\[{]/) {
    $figcmd = "\\includegraphics" ;
  } elsif (/\\begin\{overpic}/) {
    $figcmd = "\\begin{overpic}" ;
  } elsif (/\\imgexists\{/) {
    $figcmd = "\\imgexists" ;
  } else {
    return $line;
  }

  ($f = $line) =~ s/.*$figcmd[^{]*{([^}]*)}.*\n/$1/;

  # Get the full filename and the suffix
  ($fig, $ext) = scanformat($f);

  # If no suffix, use the default one
  if ($ext eq "") {
    print "Use default figure format\n";
    $ext = $figin;
  }

  # Check if this figure has been already converted
  if (exists $figdone{"$fig"}) {
    $newfig = $figdone{"$fig"};
    print "already done with $newfig\n";
    $line =~ s/$f/$newfig/g;

  # Convert the figure
  } elsif (-f $fig) {
    $fig2dev = $ext."2".$figout;
    $newfig = "fig".$figcount++.".$figout";
    for ($fig2dev) {
      /fig2eps/ && do{
        $dofig = "fig2dev -L $figout $fig > $newfig"; last;
      };
      (/fig2pdf/ || /fig2png/) && do{
        $dofig = "fig2dev -L eps $fig > tmp_fig.eps";
        $dofig .= "; ".eps2xxx("tmp_fig.eps", $newfig, $figout);
        last;
      };
      /eps2pdf/ && do{
        $dofig = eps2xxx($fig, $newfig, $figout); last;
      };
      /gif2/ && do{
        $dofig = gif2xxx($fig, $newfig, $figout); last;
      };
    }
    if ($dofig ne "") {
      $figdone{"$fig"} = $newfig;
      print "$dofig\n";
      system("$dofig");
      $line =~ s/$f/$newfig/g;
    }
  }
  return $line
}

sub parse_inlined
{
  local($line) = $_[0];
  my @texts = split("$keyon ", $line);
  my $mode = 0;
  my $nline = "";
  my $l;

  for ($i = 0; $i <= $#texts; $i++) {
    $l = $texts[$i];
    @blks = split("$keyoff ", $l);

    # Cas particulier du dernier /xt � enlever, qui est sans espace
    if ($#blks == 0) { @blks = split($keyoff, $l); }
    
    if ($i > 0) { $mode = 1; }

    # Seule la premi�re partie est � convertir
    if ($mode == 1) {
      $blks[0] = translate($blks[0]);
    }
    $nline = "$nline$blks[0]";
    if ($#blks > 0) {
      $mode = 0;
      $nline = "$nline$blks[1]";
    }
  }
  return $nline;
}

sub parse_sgml
{
  local($rawtex) = $_[0];
  local($cleantex) = $_[1];
  my $mode = 0;
  my $line = "";
  my $file = "";
  my $RTEX = "f$rawtex";
  my $CTEX = "f$cleantex";

  print "$rawtex -> $cleantex\n";

  if (-f $cleantex) {
#    print "***Warning: $cleantex already exists\n";
    system("mv $cleantex $cleantex~");
  }

  open($RTEX, "<$rawtex") || die "Cannot open $rawtex\n";
  open($CTEX, ">$cleantex") || die "Cannot open $cleantex\n";

  while (<$RTEX>) {
    $line = $_;

    # Convert the figures if needed
    $line = figconvert($line) if ($figout ne "");

    # Update the embedded commands
    command_update($line);

    if (/$keyon\n/) {
      $line =~ s/$keyon\n//;
      $mode = 1;
      chomp $line;
    } elsif ($mode == 1) {
      $line =~ s/$keyoff//;
      $line = translate($line);
    }
    if (/$keyoff\n/) {
      $line =~ s/$keyoff//;
      $mode = 0;
      chomp $line;
    } elsif (/$keyon /) {
      # More tricky, 'xt' is in the line
      $line = parse_inlined($line);
    }
    table_parse($line, $CTEX);
  }
  close($RTEX);
  close($CTEX);
}

$backend = "dvips";

getopts("f:p:b:");

if ($opt_b) {
  $backend = $opt_b;
}
# Default input figure format
if ($opt_f) {
  $figin = $opt_f;
}
if ($opt_p) {
  $path = $opt_p;
}

# The expected output figure format depends on the backend driver
for ($backend) {
  /dvips/ && do{ $figout = "eps"; last; };
  /pdftex/ && do{ $figout = "pdf"; last; };
  /none/ && do{ $figout = ""; last };
}

$rawtex = $ARGV[0];
$cleantex = basename($rawtex, '.tex');
$cleantex = dirname($rawtex). "/${cleantex}_c.tex";
shift;

if (@ARGV) {
  $cleantex = $ARGV[0];
}

parse_sgml($rawtex, $cleantex, 0);


