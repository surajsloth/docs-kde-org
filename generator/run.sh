cd /generator
python3 ./kdedocgen.py -r -g -s -c docgen_conf.ini -l doclogconfig.ini
cp /docs/public-static/* /docs/public
php ./create_generated_used.php /home/docs/docs/work > /docs/src/Data/GeneratedData.php 
cd /docs
yarn install
yarn encore dev
composer install
