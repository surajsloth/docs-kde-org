#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
# Copyright 2015  Luigi Toscano <luigi.toscano@tiscali.it>
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) version 3, or any
# later version accepted by the membership of KDE e.V. (or its
# successor approved by the membership of KDE e.V.), which shall
# act as a proxy defined in Section 6 of version 3 of the license.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library.  If not, see <http://www.gnu.org/licenses/>.

import argparse
import errno
import logging
import logging.config
import os
import shutil
from datetime import datetime

import kdedocs.configuration
import kdedocs.environments
from kdedocs.docretriever import RetrieveDocumentationBranch
from kdedocs.docgenerator import DocGenerator
from kdedocs.utils import mkdirp


LOGGER = logging.getLogger(__name__)


def copy_files(file_list, dest_dir, resource_dir):
    if not file_list:
        return
    LOGGER.info('Copying files %s to %s' % (file_list, dest_dir))
    mkdirp(dest_dir)
    for element in file_list:
        source_element = os.path.join(resource_dir, element)
        source_dir_name = os.path.basename(os.path.abspath(source_element))
        dest_path = os.path.join(dest_dir, os.path.join(dest_dir,
                                                        source_dir_name))
        if os.path.isdir(source_element):
            try:
                shutil.rmtree(dest_path)
            except OSError as exc:
                if exc.errno != errno.ENOENT:
                    raise
            shutil.copytree(source_element, dest_path)
        else:
            shutil.copy(source_element, dest_dir)


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('-v', '--verbose', action='store_true', default=False,
                        dest='verbose', help='verbose output (when a logging '
                                             'file is not specified)')
    parser.add_argument('-l', '--log-config', default='', dest='logconfigfile',
                        help='logging configuration file')
    parser.add_argument('-c', '--config-file', default='docgen_conf.ini',
                        dest='configfile', help='configuration file')
    parser.add_argument('-r', '--retrieve', action='store_true', default=False,
                        dest='retrieve', help='retrieve files')
    parser.add_argument('-g', '--generate', action='store_true', default=False,
                        dest='generate', help='generate documentation')
    parser.add_argument('-f', '--force-generate', action='store_true',
                        default=False, dest='forcegenerate',
                        help='force generation of documentation')
    parser.add_argument('-s', '--static-files', action='store_true',
                        default=False, dest='copyfiles',
                        help='copy static files to website')
    parser.add_argument('--noenv', action='store_true', default=False,
                        dest='noenv',
                        help='do not regenerate the environments')
    args = parser.parse_args()

    logging_level = logging.INFO
    if args.verbose:
        logging_level = logging.DEBUG

    if args.logconfigfile:
        curr_timestamp = datetime.now().strftime('%Y%m%d_%H%M%S')
        log_defaults = {'logfilename': 'doc_%s' % (curr_timestamp)}
        logging.config.fileConfig(args.logconfigfile, defaults=log_defaults,
                                  disable_existing_loggers=False)
    else:
        logging.basicConfig(level=logging_level,
                            format='%(asctime)s:%(levelname)s:%(name)s:'
                                   '%(message)s')
    conf = kdedocs.configuration.DocConfiguration(args.configfile)

    env_manager = kdedocs.environments.DocEnvironmentsManager(conf)
    if not args.noenv:
        env_manager.setup_all()

    if args.retrieve:
        for branch in conf.enabled_branches:
            doc_getter = RetrieveDocumentationBranch(branch, conf, env_manager)
            doc_getter.checkout_doc()

    if args.generate or args.forcegenerate:
        docgen = DocGenerator(conf, env_manager)
        docgen.generate_documentation(forced=args.forcegenerate)
        docgen.write_generated_file('generated_used.inc.php')

    if args.copyfiles:
        copy_files(conf.static_files, conf.websitedir, conf.resource_dir)

if __name__ == '__main__':
    main()
