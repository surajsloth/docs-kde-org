var Encore = require('@symfony/webpack-encore');

Encore
    // directory where compiled assets will be stored
    .setOutputPath('build/')

    // public path used by the web server to access the output path
    .setPublicPath('/build')

    .addEntry('app', './assets/js/app.js')
    .addStyleEntry('doc', './assets/css/doc.scss')

    // will require an extra script tag for runtime.js
    // but, you probably want this, unless you're building a single-page app
    .enableSingleRuntimeChunk()

    /*
     * FEATURE CONFIG
     *
     * Enable & configure other features below. For a full
     * list of features, see:
     * https://symfony.com/doc/current/frontend.html#adding-more-features
     */
    .cleanupOutputBeforeBuild()
    .enableBuildNotifications()
    .enableSourceMaps(!Encore.isProduction())

    // enables Sass/SCSS support
    .enableSassLoader()
    .autoProvidejQuery() // probably useless

    // copy icons
    .copyFiles({
        from: './assets/breeze-icons/icons/actions/22/',
        pattern: /(edit-find|languages).svg/,
        to: 'icons/[name].[ext]'
    })

    // copy image
    .copyFiles({
        from: './assets/images/',
        to: ''
    });
;

module.exports = Encore.getWebpackConfig();
