#!/bin/bash

SCRIPTDIR="$(dirname $0)"
LOGFILE="/home/docs/logs/xapianindex_$(date +%Y%m%d_%H%M).log"
echo "$SCRIPTDIR"

# Create the directory which holds the indexes
mkdir -p "${SCRIPTDIR}/omegaconf/data/default/"

make -C "${SCRIPTDIR}/omegaconf/cdb/" &>${LOGFILE}

python2 ${SCRIPTDIR}/kdedocs_indexer.py -v -l /home/docs/docs/languages \
  -d "${SCRIPTDIR}/omegaconf/data/default/" \
  -w /home/docs/website \
  &>${LOGFILE}
